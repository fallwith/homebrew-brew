## Custom Homebrew formulae

### Usage

```shell
brew tap 'fallwith/brew' 'https://gitlab.com/fallwith/homebrew-brew'
brew install <formula of choice>
```
